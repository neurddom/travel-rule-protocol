# Extensions

During our deliberations on the TRP protocol on two things consensus was
reached quickly. One is the bare minimum the FATF requires and the other that
different parties in different jurisdictions and different compliancy 
departments require different things to be exchanged.

To accomodate these views the idea of protocol Extensions was floated during
one of our illustrious Wednesday calls. The idea is simple, the core protocol
caters to the FATF Travel Rule recommendation in its purest form. All
embellishment find a home in extensions.

Using extensions helps framing discussion around securiy for example. It makes
clear what a particular detail in the protocol does and what purpose, business
or otherwise, it serves.

## Scope of extensions

Extensions can add any and all HTTP headers. 

The body of the requests can be added to at the designated location.

## Caveats

### Conflicts between extensions

With the number of extensions growing the risk of conflicts or
incompatibilities between extensions grows exponentionally. New extension
authors should take special care to investigate and address any conflicts. 

### Promotions from extension to core

Extensions might prove a comfortable testing ground for new functionality. When
an extension is universially used it could be promoted to the core protocol.

### Fallback

Implementations should cater to as many extensions as possible. Some form of
auto discovery makes sense.

## Core protocol additions

In order to support extensions some changes to the current proposal are
required.

### Api-Extensions header

The current `Api-Name` HTTP header should be renamed to a more strictly defined
`Api-Extensions` header. This header contains a comma separated list of
predefined extension names.

### Extension not supported response code

A server should be able to inform a client it is unable or unwilling to support
the extensions as defined in the client request's `Api-Extensions` header. Two
HTTP response codes are applicable, the `422 - Unprocessable Entity` and the
`501 - Unimplemented` codes. Perhaps either one of them is applicable in
different extension cases.

### Request body

The current proposal already has a top level `ivms101` key in the JSON request
body. On the same level an `extensions` key should be introduced. Each extension
would then have a key below that with specific payload. For example:

```json
{
  "ivms101": ...
  "extensions": {
    "deterministic-value-transfer": {
      "tx-id": ...,
      "vin-index": ...,
      "vout-index": ...,
    }
  }
}
```

## Extension suggestions

### Audit extension

An originator VASP might want non repudable responses from a beneficiary VASP.
Requiring signed responses would accomodate this.

### Legacy security extension

Some VASP might be unable to support the latest security best practices. This
extension would help mediate that.

### IVMS101 extension

A regulator might require additional information beyond IVMS101. That could be
handled here.
