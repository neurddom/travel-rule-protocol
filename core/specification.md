# Travel Rule Protocol

| Version | Date               | Description                                            |
| ------- | ------------------ | ------------------------------------------------------ |
| 1.0.0   | 19th August 2020   | Initial Public Version                                 |
| 1.0.1   | 10th December 2020 | Bug fix                                                |
| 1.1.0   | 8th February 2021  | Clarifications, bug fixes and consistency enhancements |
| 2.0.0   | 10th May 2021      | Addition of LNURL enhanced flow                        |
| 2.1.0   | 31st Oct 2021      | LNURL advanced flow with inquiry tag                   |
| 3.0.0   | 15th Feb 2022      | mTLS for client and server authentication              |
|         |                    | deprecate enhanced workflow                            |

## Executive Summary

Inherently, involvement within the virtual asset ecosystem presents
higher risks related to money laundering and terrorist financing than
those found in the traditional world of fiat currency. Primarily this is
due to the lack of transparency of the public keys, which are not
associated with any identity.

Whilst in the traditional world, there is proven infrastructure with
processes and tools designed to mitigate AML/CFT risks, given the
evolution of the virtual asset ecosystem, there is now a need to find
comparable solutions not only to address industry concerns but incoming
global regulations. Key operators such as custodians and market
participants must consider the following issues or gaps:

1. The current inability to identify originators and/or beneficiaries of transactions.
1. Cumbersome processes and governance to be put in place in order to confirm to
whom a defined public key belongs.

There are varying approaches across the industry to define a common set
of standards and to create a mechanism for the exchange of relevant
information including identity and associated address details all
using a common infrastructure, protocol and data set.

- [Travel Rule Protocol](#travel-rule-protocol)
  - [Executive Summary](#executive-summary)
  - [Introduction](#introduction)
  - [Goals](#goals)
    - [First Generation](#first-generation)
    - [Minimal, Workable, Pragmatic API](#minimal-workable-pragmatic-api)
    - [Minimize Fragmentation & Maximize Interoperation](#minimize-fragmentation--maximize-interoperation)
  - [Design Philosophy](#design-philosophy)
  - [Advanced Workflow](#advanced-workflow)
  - [Minimal Core Protocol with Extensions](#minimal-core-protocol-with-extensions)
    - [Security](#security)
      - [Client Authentication](#client-authentication)
  - [Technical implementation - General](#technical-implementation-general)
    - [Core Protocol](#core-protocol)
    - [Headers](#headers)
      - [Sample Header](#sample-header)
    - [Extensions](#extensions)
      - [Scope](#scope)
      - [Caveats](#caveats)
        - [Conflicts between extensions](#conflicts-between-extensions)
        - [Promotions from extension to core](#promotions-from-extension-to-core)
        - [Fallback](#fallback)
        - [Core protocol additions](#core-protocol-additions)
          - [API-Extensions header](#api-extensions-header)
          - [Extension not supported response code](#extension-not-supported-response-code)
          - [Request body](#request-body)
  - [Technical implementation - Advanced Workflow](#technical-implementation-advanced-workflow)
    - [Motivation](#motivation)
    - [Overview](#overview)
    - [Detailed advanced workflow](#detailed-advanced-workflow)
      - [Response data](#response-data)
    - [Transfer Inquiry Resolution](#transfer-inquiry-resolution)
      - [Response data](#response-data)
    - [Transfer Confirmation](#transfer-confirmation)
      - [Response data](#response-data)
    - [General error handling](#general-error-handling)
      - [Invalid/Expired URLs](#invalidexpired-urls)
      - [Error on server side](#error-on-server-side)
  - [Compliance with FATF Recommendation 16](#compliance-with-fatf-recommendation-16)
  - [Authoritative Version](#authoritative-version)
  - [References](#references)
  - [Founding Organizations](#founding-organizations)
    - [List of Contributors in Alphabetic Order by Organization](#list-of-contributors-in-alphabetic-order-by-organization)

## Introduction

The Travel Rule working group was primarily established to address a
specific recommendation issued by the Financial Action Task Force
("FATF"). Members of the working group include leading industry
participants from around the world who meet regularly to discuss and
agree on finding a common solution to meet the regulatory standards. The
information within this paper seeks to highlight a solution, referred
herein as the Travel Rule Protocol (TRP).

The TRP vision is to define a set of standards which will allow Virtual
Asset Service Providers (VASPs - as defined as in footnote[^1] below)
to safely share with other trusted participants the identity of an
originator and beneficiary linked to a specific virtual asset transfer.
This set of standards aims to meet the requirements set by the FATF
Recommendation 16 (R16), commonly known as the Travel Rule defined as
below:

> R16 -- Countries should ensure that originating VASPs obtain and
> hold required and accurate originator information and required
> beneficiary information on virtual asset transfers, submit the
> above information to the beneficiary VASP or financial institution
> (if any) immediately and securely, and make it available on
> request to appropriate authorities. Countries should ensure that
> beneficiary VASPs obtain and hold required originator information
> and required and accurate beneficiary information on virtual asset
> transfers, and make it available on request to appropriate
> authorities. Other requirements of R16 (including monitoring of
> the availability of information, and taking freezing action and
> prohibiting transactions with designated persons and entities)
> apply on the same basis as set out in R16. The same obligations
> apply to financial institutions when sending or receiving virtual
> asset transfers on behalf of a customer.

It is intended that adoption of the TRP is royalty and license free.
However, any fees and licenses related to implementation, service of
software providers, etc. will be the responsibility of the implementing
VASP, including but not limited to document the legal and contractual
arrangement ensuring that any exchange of information is done in
compliance with applicable laws, e.g. GDPR.

The TRP has been incrementally developed by a self-selecting group of
leading industry participants from around the world, collaborating
openly and sharing to develop and evolve the solution. There is no
single central body or organization behind the protocol, only
collaboration.

The TRP working group has a regular weekly minuted meeting with a set
agenda. There are other forums including chat rooms for more frequent
collaboration as well as email for more infrequent formal announcements.
The working group welcomes all contributions, ideas and input to the
design and implementation of the protocol. Industry participants who
wish to help developing the protocol, or adopt the protocol are
encouraged to register at [https://travelruleprotocol.org/](https://travelprotocol.org)

Any comments, corrections or suggestions on this version of the protocol
specification can also be captured via the feedback form at
[https://travelruleprotocol.org](https://travelruleprotocol.org)

## Goals

The goal of the TRP is to develop a first-generation, minimal, workable,
and pragmatic API specification for compliance with the FATF Travel Rule
in a timely manner, that also minimizes fragmentation and maximizes
interoperability.

### First Generation

Industry participants acknowledge that mainstream adoption of virtual
assets as an asset class will take time, and encouragingly there is
evidence that its evolution is progressing with increasing focus by
regulators and central banks imposing regulatory requirements. The TRP
aims to initiate one of the first steps on this journey by launching a
first-generation solution for compliance with the FATF Travel Rule.

### Minimal, Workable, Pragmatic API

The TRP has been developed with sufficient flexibility allowing
straightforward adoption for a wide range of industry participants. By
focusing closely on FATF R16 and limiting the scope of the solution, the
TRP aims to create a simple API specification for required information
exchange that will seamlessly integrate with existing business IT
frameworks.

### Minimize Fragmentation & Maximize Interoperation

There are a number of potentially more comprehensive Travel Rule
solutions in development across the industry participants. By focusing
closely on FATF R16 and limiting the scope of the solution, the TRP aims
to create a simple API specification for the information exchange that
is easy to implement and integrate with existing business solutions, as
well as having an easy path to interoperate with other emerging industry
Travel Rule solutions as they mature. Any investment made should be
transferable to future industry solutions.

## Design Philosophy

The underlying design principles of the Travel Rule Protocol:

1. Compliance with FATF R16
1. Data privacy: Bilateral exchange for minimal data leakage
1. Iterative and incremental with early MVP launch
1. Open for industry collaboration and adoption
1. Aligned with emerging industry standards such as IVMS101
1. Any investment made should be transferable to future industry
solutions
1. Each member controls the data that they share

## Advanced Workflow

In the advanced workflow, the originator VASP receives a special
payment address, called Travel Address, instead of a normal blockchain address from the end user.
This Travel Address is a bech32 encoded URL which contains a unique URL.
Bech32 encoded URLs are standardized as LNURL[^2]. Once the LNURL
is decoded by the originator VASP a request is made to that URL to
inquire for permission to execute an transaction.

This workflow hence allows to:

* decline transfers
* conduct KYC checks
* easily share transaction ids

which solves many practical problems for compliance teams.

## Minimal Core Protocol with Extensions

The first part of the goals of the TRP:

> "first generation, minimal, workable, and pragmatic API specification
> for compliance with the FATF Travel Rule in a timely manner"

is centered around having a simple minimal protocol that is easy to
implement and to comply with.

The second part of the goals is to:

> "minimize fragmentation and maximize interoperation"

In order to meet both of these goals the TRP is separated into two
parts:

1. A minimal core protocol
1. Extensions

The minimal core protocol should contain mandatory elements that cover
the bare minimum FATF R16 requirements. This meets the first part of the
goals.

However, it is acknowledged that one size does not fit all, taking into
account differing jurisdictional and compliance requirements on what
information is to be exchanged. To help accommodate the second goal an
extension mechanism is proposed.

The minimum core protocol will comply with the mandatory elements of
R16, all other requirements will be added using the extension mechanism.

### Security

TRP uses TLS 1.3 to secure all protocol traffic. This is a
well-understood and widely-adopted solution for communication
security.

#### Client Authentication

Clients are authenticated using a X.509 certificate. Those are
most commonly used to authenticate a server when using TLS.

It is referred to as *mutual TLS* ([mTLS](https://en.wikipedia.org/wiki/Mutual_authentication#mTLS))
when both, client *and* server, are authenticated using X.509
certificates.

In TRP, a server's client certificate can be obtained by a `GET`
request to the `/identity` endpoint. It is returned in a JSON
document to allow for future extension. The expected response
looks as following:

```text
HTTP 200

{
    "x509": [x509 certificate in PEM format]
}
```

The PEM format is of type `string`. Further details can be obtained
[here](https://en.wikipedia.org/wiki/Privacy-Enhanced_Mail).

TRP servers are required to reject connections from clients that
cannot authenticate themselves using mTLS. The only exception is the
`/identity` endpoint.

A VASP is free to choose any [CA provider](https://en.wikipedia.org/wiki/Certificate_authority#Providers).
In TRP, mTLS is used purely for establishing a secure communication channel.
There is no additional semantics attached to the X509 certificates.

Because mTLS is established and well-understood it can be used
without custom implementation out-of-the-box with
[nginx](https://www.nginx.com/nginxconf/2019/session/mtls-nginx/).

## Technical implementation - General

### Core Protocol

Each VASP who wants to be compliant with this specification must expose two
RESTful endpoints. In addition, and entirely optional, zero or more extensions
can be supported. Users of the TRP protocol should agree on a bilateral basis
on which extensions they will use.

### Headers

Each request must have the following header fields:

| Field                | Description                                                                                                         | Example value                                  |
| -------------------- | ------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| `api-version`        | Version of the API                                                                                                  | `1.0.0`                                        |
| `api-extensions`     | Comma separated list of extensions to the base the TRP protocol to be used when processing this request.[1](#note1) | `request-signing,beneficiary-details-response` |
| `request-identifier` | Unique UUID V4 value for this particular API call (different for all calls)                                         | `2351f3f1-bcff-4a06-a07a-8de94220a9b0`         |

<a name="note1">1</a>: Use of extensions is  optional and must be agreed on between parties. A
server should be able to inform  a client it is unable or unwilling to support
the extensions as defined in the  client request's `api-extensions` header. Two
HTTP response codes are  applicable, the `422 - Unprocessable Entity` and the
`501 - Unimplemented` codes.  Either one of them is applicable in different
extension cases. See "Extensions"  section of the document for more details.

Each response must include the headers `api-version` and `request-identifier`.
The former holds the version of the API the responder is using, the latter
echoes the `request-identifier` back to the sender.

#### Sample Header

```txt
api-version: 1.0.0
api-extensions: request-signing,beneficiary-details-response
request-identifier: 2351f3f1-bcff-4a06-a07a-8de94220a9b0
```

### Extensions

#### Scope

Extensions can add any and all HTTP headers or parameters.

The body of the requests can be added to the designated location.

#### Caveats

##### Conflicts between extensions

With the number of extensions growing the risk of conflicts or
incompatibilities between extensions grows exponentially. New extension
authors should take special care to investigate and address any
conflicts.

##### Promotions from extension to core

Extensions might prove a comfortable testing ground for new
functionality. When an extension is universally used it could be
promoted to the core protocol. The promotion mechanism is still to be
decided.

##### Fallback

Implementations should cater to as many extensions as possible. Some
form of auto-discovery and if practical fallback should be catered for.

##### Core protocol additions

In order to support extensions some extension-specific handling is
required in the core protocol beyond what is necessary for R16
compliance. These are described in the following section.

###### API-Extensions header

This header contains a comma separated list of predefined extension
names. It must be omitted when no extensions are enabled.

###### Extension not supported response code

A server should be able to inform a client that it is unable or unwilling to
support the extensions as defined in the client request's
`api-extensions` header. Two HTTP response codes are applicable, the `422 -
Unprocessable Entity` and the `501 - Not Implemented`. Either one of them
is applicable in different extension cases.

###### Request body

Extensions have a reserved top level key in JSON payloads called `extensions`.
Each extension then has a key below that with a specific payload. For example:

```txt
{
  "IVMS101": ...
  "extensions": {
    "offchainData": {
      "refId": ...,
    }
  }
}

```

When no extensions are used in the request, i.e. the `api-extensions` header is
absent, the key `extensions` must be omitted.

## Technical implementation - Advanced Workflow

### Motivation

The advanced workflow solves the following issues:

1. enables to decline transactions _a priori_ by using a transfer
   inquiry. This reduces the workload significantly for the
   compliance team.

2. enables to negotiate a payment address _after_ the KYC checks
   have been completed. This eliminates _most_ unwanted payments.

3. enables to share the transaction id _after_ the blockchain
   payment has been executed by using the transfer confirmation.
   This eliminates the need for signed but unbroadcasted transactions
   that require low-level tinkering and are risky because they
   can be executed by simply broadcasting them. The transaction
   id is useful for the beneficiary to check if he has received
   the payment and trigger a human interaction in case it wasn't
   received.

### Overview

![overview](overview.png)

### Detailed advanced workflow

An example advanced LNURL looks like this:

```
LNURL1DP68GURN8GHJ7UM9WFMXJCM99E3K7MF0V9CXJ0M385EKVCENXC6R2C35XVUKXFN5V9NN6ARJV9MX2MZJW4KX2JTWW96KJUNEVXPY3S
```

It encodes:

```
https://beneficiary.com/implementation/defined/path&tag=travelRuleInquiry
```

The `tag` query parameter is mandatory.
According to the [LNURL spec](https://github.com/fiatjaf/lnurl-rfc/blob/luds/01.md)
the `tag` query parameter indicates special meaning. In this case the presence of
`travelRuleInquiry` means that a POST request should be made containing the following body.

```txt
{
    "asset": {
        "slip0044": [asset identifier as per SLIP-0044],
    },
    "amount": [amount in lowest denomination of asset],
    "callback": "https://originator.com/implementation/defined/path/for/inquiryResolution",
    "IVMS101": {
        "originator": {
            "originatorPersons": [
                {
                    "naturalPerson": {
                        "name": {
                            "nameIdentifier": [
                                {
                                    "primaryIdentifier": "Post",
                                    "secondaryIdentifier": "Johnny",
                                    "nameIdentifierType": "LEGL"
                                }
                            ]
                        },
                        "geographicAddress": [
                            {
                                "addressType": "GEOG",
                                "streetName": "Potential Street",
                                "buildingNumber": "123",
                                "buildingName": "Cheese Hut",
                                "postCode": "91361",
                                "townName": "Thousand Oaks",
                                "countrySubDivision": "California",
                                "country": "US"
                            }
                        ],
                        "customerIdentification": "1002390"
                    }
                }
            ]
        },
        "beneficiary": {
            "beneficiaryPersons": [
                {
                    "naturalPerson": {
                        "name": {
                            "nameIdentifier": [
                                {
                                    "primaryIdentifier": "MachuPichu",
                                    "secondaryIdentifier": "Freddie",
                                    "nameIdentifierType": "LEGL"
                                }
                            ]
                        }
                    }
                }
            ]
        },
        "originatingVASP": {
            "originatingVASP": {
                "legalPerson": {
                    "name": {
                        "nameIdentifier": [
                            {
                                "legalPersonName": "VASP A",
                                "legalPersonNameIdentifierType": "LEGL"
                            }
                        ]
                    },
                    "nationalIdentification": {
                        "nationalIdentifier": "506700T7Z685VUOZL877",
                        "nationalIdentifierType": "LEIX"
                    }
                }
            }
        }
    }
}
```

SLIP-0044 is available [here](https://github.com/satoshilabs/slips/blob/master/slip-0044.md).
The JSON is structured such that it can be easily extended to
support other asset identifier standards in the future.

#### Response data

HTTP OK 200

```txt
{
    "version": "2.1.0"
}
```

### Transfer Inquiry Resolution

The callback URL from the transfer inquiry is used to send the
following POST request

```json
{
    "approved": {
        "address": "some payment address",
        "callback": "https://beneficiary.com/implementation/defined/path/for/transferConfirmation"
    }
}
```

or

```txt
{
    "rejected": "human readable comment" [or null]
}
```

#### Response data

HTTP 204 No Content

### Transfer Confirmation

The callback URL from the transfer inquiry resolution is used to send the
following POST request

```txt
{
    "txid": [some asset-specific tx identifier]
}
```

or

```txt
{
    "canceled": "human readable comment" [or null]
}
```

The `txid` should only be communicated if the transaction has been confirmed on-chain.

#### Response data

HTTP 204 No Content

### General error handling

#### Invalid/Expired URLs

An LNURL can expire (e.g. customer closes account) or be invalid
(e.g. someone crafts a URL with arbitrary parameters). Also a
callback URL can expire (e.g. already called or timed out) or
be invalid by construction. A backend must reply in such a case
with an HTTP status code 404 NOT FOUND.

#### Error on server side

If an error is triggered on the server side (e.g. database unreachable)
then a backend must reply with HTTP status code 500 INTERNAL SERVER ERROR.


## Compliance with FATF Recommendation 16

The key part of the FATF VASP Recommendations is:

> A.  As described in INR.15, paragraph 7(b), all of the requirements set
> forth in Recommendation 16 apply to VASPs or other obliged
> entities that engage in VA transfers, including the obligations to
> obtain, hold, and transmit required originator and beneficiary
> information in order to identify and report suspicious
> transactions, monitor the availability of information, take
> freezing actions, and prohibit transactions with designated
> persons and entities.

*By exchanging originator and beneficiary information between VASPs,
the TRP facilitates compliance with this requirement. Information must
be stored by each VASPs themselves. No central or distributed database
is proposed.*

> A.  Further, countries should ensure that beneficiary institutions
> (whether a VASP or other obliged entity) obtain and hold required
> (not necessarily accurate) originator information and required and
> accurate beneficiary information, as set forth in INR. 16. The
> required information includes the:
>
> 1. originator's name (i.e., the sending customer);
*The TRP facilitates compliance with this requirement.*
> 1. originator's account number where such an account is used to process
> the transaction (eg: the VA wallet); *The TRP facilitates compliance with
>  this requirement. The account is the public address.*
> 1. originator's physical (geographical) address, or national identity
> number, or customer identification number (i.e., not a transaction
> number) that uniquely identifies the originator to the ordering
> institution, or date and place of birth; *The TRP facilitates compliance with
>  this requirement. The TRP working group has agreed to use LEI or GEOG from
> IVMS101 in first phase.*
> 1. beneficiary's name; and *The TRP facilitates compliance with this requirement.*
> 1. beneficiary account number where such an account is used to process
> the transaction (eg: the VA wallet). It is not necessary for the
> information to be attached directly to the VA transfer itself. The
> information can be submitted either directly or indirectly, as set
> forth in INR. 15. *The TRP facilitates compliance with this requirement. The
> account is the public address.*

## Authoritative Version

This document provides a description of the TRP and its
usage. Where possible this document will be updated and new versions
issued as the protocol is revised.

## References

- GDF Taxonomy for Cryptographic Asset Proofs
  [https://www.gdf.io/wp-content/uploads/2018/10/0010\_GDF\_Taxonomy-for-Cryptographic-Assets\_Proof-V2-260719.pdf](https://www.gdf.io/wp-content/uploads/2018/10/0010_GDF_Taxonomy-for-Cryptographic-Assets_Proof-V2-260719.pdf)
- IVMS101: Joint Working Group for interVASP Messaging Standards
  [www.intervasp.org](http://www.intervasp.org)
- FATF Guidance for a Risk-Based Approach to Virtual Assets and
  Virtual Asset Service Providers, particularly points 111 to 119 in
  the context of the TRP:
  [https://www.fatf-gafi.org/publications/fatfrecommendations/documents/guidance-rba-virtual-assets.html](https://www.fatf-gafi.org/publications/fatfrecommendations/documents/guidance-rba-virtual-assets.html)

## Founding Organizations

The Travel Rule Protocol working group was founded by and the first
draft version of the TRP protocol was created by Standard Chartered, ING
and BitGo. Through welcoming open industry collaboration the working
group has since grown to encompass a large number of leading
organizations from across the global Virtual Asset Industry and
continues to welcome further organizations to join.

The working group is currently chaired by Andrew Davidson, CTO of OSL.

[^1]: See definition of Virtual Asset Service Providers at <https://www.fatf-gafi.org/glossary/u-z/>
[^2]: <https://github.com/fiatjaf/lnurl-rfc>

### List of Contributors in Alphabetic Order by Organization

| Name                 | Organization                         |
| -------------------- | ------------------------------------ |
| Harm Aarts           | 21 Analytics AG                      |
| Lucas Betschart      |                                      |
| Filip Gospodinov     |                                      |
| Adedeji Owonibi      | A&D Forensics                        |
| Andrew Davidson      | BC Group / OSL                       |
| Hugh Madden          |                                      |
| Nathan Simmons       |                                      |
| Usman Ahmad          |                                      |
| Ben Chan             | BitGo                                |
| Chris Metcalfe       |                                      |
| Kiarash Mosayeri     |                                      |
| Dave Jevans          | CipherTrace                          |
| John Jefferies       |                                      |
| Charlene Cieslik     | Complifact AML Inc.                  |
| Magdalena Boškić     | Crypto Finance AG                    |
| Nathaniel Zollinger  |                                      |
| Malcolm Wright       | Diginex / EQUOS.io                   |
| Ray Hennessey        |                                      |
| Jack Gavigan         | Electric Coin Company                |
| David Carlisle       | Elliptic                             |
| James Byrne          | Digivault                            |
| Sheehan Anderson     | Fidelity Digital Assets              |
| Ekaterina Anthony    | Geissbühler Weber & Partner (gwp)    |
| Ben El-Baz           | HashKey                              |
| Alessio Quaglini     | Hex Trust                            |
| Rafal Czerniawski    |                                      |
| Simon Lee            | Hodlnaut Pte. Ltd.                   |
| Antoine Girard       | ING                                  |
| Herve Francois       |                                      |
| Kaloyan Tanev        |                                      |
| Susan Patterson      | Komainu (Jersey) Limited             |
| Benjamin Usinger     | KPMG Advisory                        |
| Edmund Lowell        | KYC-Chain                            |
| Adrien Treccani      | Metaco                               |
| Thomas Hardjono      | MIT Connection Science & Engineering |
| Justin Newton        | Netki                                |
| Andrés Junge         | Notabene                             |
| Pelle Braendgaard    |                                      |
| Megan Monroe-Coleman | OKCoin                               |
| Alexandre Kech       | Onchain Custodian                    |
| Raymond              |                                      |
| Javier Tamashiro     | Ospree                               |
| Jukka Pellinen       |                                      |
| Lana Schwartzman     | Paxful                               |
| Peter Davey          | Peter Davey and Associates Limited   |
| Jo Lee               | Standard Chartered Bank              |
| Maxime de Guillebon  |                                      |
| Thierry Janaudy      |                                      |
| Duncan Trenholme     | TP ICAP                              |
| Simon Forster        |                                      |
| Dave Jevans          | Trisa                                |
| John Jefferies       |                                      |
| Neil Samtani         | XReg                                 |
| Siân Jones           |                                      |

**End of Document**
